const express = require("express"),
  bodyParser = require("body-parser"),
  jsonPayloadHandler = require("./util/json-payload-handler"),
  app = express();

//Server Configuration
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use((err, req, res, next) => {
  if (err instanceof SyntaxError) {
    //assuming the json request is not a validated request
    res
      .status(400)
      .send({ error: "Could not decode request: JSON parsing failed" });
  } else {
    next();
  }
});

const port = process.env.PORT || 7070;

const router = express.Router();

router.get("/", (req, res) => {
  res.json({ message: "Welcome to my Awesome API (ver 2.0) !! ^_^" });
});

router.post("/", ({ body }, res) => {
  //  console.log('Body contents: ', req.body);
  const jsonRequestBody = body;

  //call utility jsonPayloadHandler function
  jsonPayloadHandler.parsePayload(jsonRequestBody, (err, jsonData) => {
    if (err) {
      res.status(400).send(err);
      return;
    }
    res.json(jsonData);
  });
});

app.use("/", router);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log(`Channel 9 Express API server listening on ${port}`);
