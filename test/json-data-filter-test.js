const should = require("should");
const jsonDataFilter = require("../util/json-data-filter");

describe("jsonDataFilter", () => {
  describe("checkPayloadIfValid(jsonBody)", () => {
    it("should return true if payload is valid", () => {
      const jsonBody = {
        payload: [
          {
            foo: "bar"
          }
        ]
      };

      jsonDataFilter.checkPayloadIfValid(jsonBody).should.be.true;
    });

    it("should return false if payload is invalid ", () => {
      const jsonBody = undefined; //undefined json

      jsonDataFilter.checkPayloadIfValid(jsonBody).should.be.false;
    });
  });

  describe("checkDataArrayByParams(jsonBody, drm, episodeCount)", () => {
    it("should return one JSON object array with drm param being true and episodeCount param greater than 0 ", () => {
      const jsonBody = {
        payload: [
          {
            country: "UK",
            description:
              "What's life like when you have enough children to field your own football team?",
            drm: true,
            episodeCount: 3
          }
        ]
      };

      const drm = true;
      const episodeCount = 0;

      const payloadLength = jsonDataFilter.checkDataArrayByParams(
        jsonBody,
        drm,
        episodeCount
      );

      payloadLength.should.be.an.instanceOf(Object);
      payloadLength.should.be.above(0);
    });

    it("should return an zero-length JSON object array if drm param is false", () => {
      const jsonBody = {
        payload: [
          {
            description:
              "What's life like when you have enough children to field your own football team?",
            drm: false,
            episodeCount: 3
          }
        ]
      };
      const drm = true;
      const episodeCount = 0;

      const payloadLength = jsonDataFilter.checkDataArrayByParams(
        jsonBody,
        drm,
        episodeCount
      );

      payloadLength.should.be.an.instanceOf(Object);
      payloadLength.should.be.equal(0);
    });

    it("should return an zero-length JSON object array if episodeCount less than 0", () => {
      const jsonBody = {
        payload: [
          {
            description:
              "What's life like when you have enough children to field your own football team?",
            drm: true,
            episodeCount: -1
          }
        ]
      };

      const drm = true;
      const episodeCount = 0;

      const payloadLength = jsonDataFilter.checkDataArrayByParams(
        jsonBody,
        drm,
        episodeCount
      );

      payloadLength.should.be.an.instanceOf(Object);
      payloadLength.should.be.equal(0);
    });

    it("should return false if an error thrown of invalid JSON Data", () => {
      const jsonBody = undefined;

      const drm = true;
      const episodeCount = 0;

      const invalidPayload = jsonDataFilter.checkDataArrayByParams(
        jsonBody,
        drm,
        episodeCount
      );

      invalidPayload.should.be.an.instanceOf(Object);
      invalidPayload.should.be.equal(false);
    });
  });

  describe("selectDataArrayByFields(image, title, slug)", () => {
    it("should return a json object array with image, title and slug property fields ", () => {
      const jsonBodyArray = [
        {
          country: "UK",
          description:
            "What's life like when you have enough children to field your own football team?",
          drm: true,
          episodeCount: 3,
          genre: "Reality",
          image: {
            showImage:
              "http://catchup.ninemsn.com.au/img/jump-in/shows/16KidsandCounting1280.jpg"
          },
          language: "English",
          nextEpisode: null,
          primaryColour: "#ff7800",
          seasons: [
            {
              slug: "show/16kidsandcounting/season/1"
            }
          ],
          slug: "show/16kidsandcounting",
          title: "16 Kids and Counting",
          tvChannel: "GEM"
        }
      ];

      jsonDataFilter.setJsonObjectArray(jsonBodyArray); // code-smell or bad design
      const jsonResponseSet = jsonDataFilter.selectDataArrayByFields(
        "image",
        "title",
        "slug"
      );

      jsonResponseSet.should.be.instanceOf(Array);
      jsonResponseSet.length.should.be.above(0);
      jsonResponseSet[0].should.have.properties("image", "title", "slug");
    });

    it("should return an empty or undefined json object  if no image, title or slug property fields are found)", () => {
      const jsonBodyArray = [
        {
          country: "UK",
          description:
            "What's life like when you have enough children to field your own football team?",
          drm: true,
          episodeCount: 3,
          genre: "Reality",
          language: "English",
          nextEpisode: null,
          primaryColour: "#ff7800",
          seasons: [
            {
              slug: "show/16kidsandcounting/season/1"
            }
          ],
          tvChannel: "GEM"
        }
      ];

      jsonDataFilter.setJsonObjectArray(jsonBodyArray); // code-smell or bad design
      const jsonResponseSet = jsonDataFilter.selectDataArrayByFields(
        "image",
        "title",
        "slug"
      );

      jsonResponseSet.should.be.instanceOf(Object);
      jsonResponseSet.length.should.be.above(0);
      jsonResponseSet.should.not.have.properties("image", "title", "slug");
    });
  });
});
