const should = require("should"),
  nock = require("nock"),
  jsonPayloadHandler = require("../util/json-payload-handler");

describe("jsonPayloadHandler", () => {
  describe("parsePayload(jsonRequestBody, callback)", () => {
    it("should parse json payload when json is valid", () => {
      const jsonData = {
        payload: [
          {
            country: "UK",
            description:
              "What's life like when you have enough children to field your own football team?",
            drm: true,
            episodeCount: 3,
            genre: "Reality",
            image: {
              showImage:
                "http://catchup.ninemsn.com.au/img/jump-in/shows/16KidsandCounting1280.jpg"
            },
            language: "English",
            nextEpisode: null,
            primaryColour: "#ff7800",
            seasons: [
              {
                slug: "show/16kidsandcounting/season/1"
              }
            ],
            slug: "show/16kidsandcounting",
            title: "16 Kids and Counting",
            tvChannel: "GEM"
          }
        ]
      };

      nock("http://localhost:7070")
        .post("/", jsonData)
        .reply(200);

      jsonPayloadHandler.parsePayload(jsonData, (error, jsonData) => {
        if (error !== null) {
          error.should.not.be.ok;
        }

        should.exist(jsonData);
      });
    });

    it("should return 400 error when failed to parse json due to external factors(ie server timeout errors, or empty json object)", () => {
      const jsonData = null || {}; //pretend that json data is empty object

      nock("http://localhost:7070")
        .post("/", jsonData)
        .reply(400);

      jsonPayloadHandler.parsePayload(jsonData, (error, jsonData) => {
        should.exist(error);
        should.not.exist(jsonData);
      });
    });
  });
});
