# Channel9 Coding Test

JSON Web Service API written in NodeJS

## Libraries Used

1. Express
2. Mocha
3. Body Parser

## Prerequisites

Make sure lates Node and Yarn versions must be installed first on your machine.

## Installation

Run ~~`npm install`~~ or `yarn install`

## Usage

1. To run the web service, run ~~`npm start`~~ or `yarn start`.
2. To run some functional and tests, run ~~`npm test`~~ or `yarn test`.

## Deployment

The app is currently deployed to Heroku environment - https://devcenter.heroku.com/articles/deploying-nodejs 