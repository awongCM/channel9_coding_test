let jsonObjectArray = [];

/* check if the payload is valid json */
function checkPayloadIfValid(jsonBody) {
  let parsedJSONValid = [];

  try {
    parsedJSONValid = JSON.parse(JSON.stringify(jsonBody));
    if (parsedJSONValid) {
      return true;
    } else {
      return false;
    }
  } catch (err) {
    return false; //invalid json
  }
}

/* check payload array for these params and return a new filtered array */
function checkDataArrayByParams(jsonBody, drm, episodeCount) {
  try {
    jsonObjectArray = [];
    jsonObjectArray = jsonBody.payload; //expecting payload property

    jsonObjectArray = jsonObjectArray.filter(item => {
      if (item.drm === drm && item.episodeCount > episodeCount) {
        return true;
      }
    });
    return jsonObjectArray.length; //assuming we have items in the filtered array(and valid)
  } catch (err) {
    return false; //invalid json
  }
}

/* once filtered, return the new payload with these main properties we're interested */
function selectDataArrayByFields(image, title, slug) {
  try {
    let jsonResultSet = [];

    if (jsonObjectArray) {
      jsonResultSet = jsonObjectArray.map(item => {
        if (
          item.hasOwnProperty(image) &&
          item.hasOwnProperty(title) &&
          item.hasOwnProperty(slug)
        ) {
          const newItem = {
            image: item.image.showImage,
            slug: item.slug,
            title: item.title
          };
          return newItem;
        }
      });
    }
    return jsonResultSet;
  } catch (err) {
    return false; //invalid json
  }
}

function setJsonObjectArray(jsonOAValue) {
  jsonObjectArray = jsonOAValue;
}

module.exports = {
  jsonObjectArray: jsonObjectArray,
  checkPayloadIfValid: checkPayloadIfValid,
  checkDataArrayByParams: checkDataArrayByParams,
  selectDataArrayByFields: selectDataArrayByFields,
  setJsonObjectArray: setJsonObjectArray
};
