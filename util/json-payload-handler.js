var jsonDataFilter = require("./json-data-filter");

function parsePayload(jsonRequestBody, callback) {
  const drm = true;
  const episodeCount = 0;
  let jsonResponseSet = [];

  //call jsonDataFilter functions(along with their callbacks)
  if (!jsonDataFilter.checkPayloadIfValid(jsonRequestBody)) {
    callback({ error: "Could not decode request: JSON parsing failed" }, null);
    return;
  }

  //2.checkDataArrayByParams
  if (
    !jsonDataFilter.checkDataArrayByParams(jsonRequestBody, drm, episodeCount)
  ) {
    callback({ error: "Could not decode request: JSON parsing failed" }, null);
    return;
  }

  //3. filterDataArrayByFields
  jsonResponseSet = jsonDataFilter.selectDataArrayByFields(
    "image",
    "title",
    "slug"
  );

  if (jsonResponseSet) {
    callback(null, { response: jsonResponseSet });
    return;
  } else {
    callback({ error: "Could not decode request: JSON parsing failed" }, null);
    return;
  }
}

module.exports = {
  parsePayload: parsePayload
};
